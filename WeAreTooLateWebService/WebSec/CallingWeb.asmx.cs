﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace WebSec
{
    /// <summary>
    /// Summary description for CallingWeb
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CallingWeb : System.Web.Services.WebService
    {
        //Local variables
        string apiKey = "74ae6140350ce5d9ab49a9e4c836cecf";
        string url = "https://api.darksky.net/forecast/";

        [WebMethod]
        public double LisbonNow()
        {
            //Local Variables
            double temperatureF;
            string temperature = "";

            //Latitude and longitude
            double latitude = 38.7166700;
            double longitude = -9.1333300;

            //Exclude flags on the api and construct of the link
            string excludeFlags = "?exclude=flags,alerts,daily,hourly,minutely";
            string latitudeLongitude = latitude.ToString() + "," + longitude.ToString();
            string apiLatitude = apiKey.ToString() +"/" + latitudeLongitude + excludeFlags;
            string body = "74ae6140350ce5d9ab49a9e4c836cecf/38.7166700,-9.1333300";

            //Creating the client in rest 
            var client = new RestClient(url);
            var rest = new RestRequest(body, Method.GET);
            
            //Creating a response and contente
            IRestResponse response = client.Execute(rest);
            var content = response.Content;

            //Passing content to the string
            temperature = response.Content;

            //Dynamic json response
            dynamic jsonResponse = JsonConvert.DeserializeObject(response.Content);
            
            //getting the temperature from the api
            var temperatureApi = jsonResponse.currently.temperature;
            
            //Getting the value of the temperature in api
            temperature = temperatureApi.ToString();

            //Converting the temperature in double
            temperatureF = Convert.ToDouble(temperature);

            //Converting f to celsius
            temperatureF = (temperatureF - 32) / 1.8;

            //Returing the value
            return temperatureF;
        }

        [WebMethod]
        public double FaroNow()
        {
            //Local Variables
            double temperatureF;
            string temperature = "";

            //Latitude and longitude
            double latitude = 37.0193700;
            double longitude = -7.9322300;
            
            //Exclude flags on the api and construct of the link
            string excludeFlags = "?exclude=flags,alerts,daily,hourly,minutely";
            string latitudeLongitude = latitude.ToString() + "," + longitude.ToString();
            string apiLatitude = apiKey.ToString() + "/" + latitudeLongitude + excludeFlags;
            string body = "74ae6140350ce5d9ab49a9e4c836cecf/38.7166700,-9.1333300";

            //Creating the client in rest 
            var client = new RestClient(url);
            var rest = new RestRequest(body, Method.GET);

            //Creating a response and contente
            IRestResponse response = client.Execute(rest);
            var content = response.Content;

            //Passing content to the string
            temperature = response.Content;

            //Dynamic json response
            dynamic jsonResponse = JsonConvert.DeserializeObject(response.Content);

            //getting the temperature from the api
            var temperatureApi = jsonResponse.currently.temperature;

            //Getting the value of the temperature in api
            temperature = temperatureApi.ToString();

            //Converting the temperature in double
            temperatureF = Convert.ToDouble(temperature);

            //Converting f to celsius
            temperatureF = (temperatureF - 32) / 1.8;

            //Returing the value
            return temperatureF;
        }

        [WebMethod]
        public double PortoNow()
        {
            //Local Variables
            double temperatureF;
            string temperature = "";

            //Latitude and longitude
            double latitude = 41.1496100;
            double longitude = -8.6109900;

            //Exclude flags on the api and construct of the link
            string excludeFlags = "?exclude=flags,alerts,daily,hourly,minutely";
            string latitudeLongitude = latitude.ToString() + "," + longitude.ToString();
            string apiLatitude = apiKey.ToString() + "/" + latitudeLongitude + excludeFlags;
            string body = "74ae6140350ce5d9ab49a9e4c836cecf/38.7166700,-9.1333300";

            //Creating the client in rest 
            var client = new RestClient(url);
            var rest = new RestRequest(body, Method.GET);

            //Creating a response and contente
            IRestResponse response = client.Execute(rest);
            var content = response.Content;

            //Passing content to the string
            temperature = response.Content;

            //dynamic json response
            dynamic jsonResponse = JsonConvert.DeserializeObject(response.Content);

            //getting the temperature from the api
            var temperatureApi = jsonResponse.currently.temperature;

            //Getting the value of the temperature in api
            temperature = temperatureApi.ToString();

            //Converting the temperature in double
            temperatureF = Convert.ToDouble(temperature);

            //Converting f to celsius
            temperatureF = (temperatureF - 32) / 1.8;

            //Returing the value
            return temperatureF;
        }

        [WebMethod]
        public double PortoAvarageToday()
        {
            //Variables
            double tempPorto = 0;
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=WeAreTooLate;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Seing date time today
            DateTime today = DateTime.Today.Date;

            //SqlConnection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Command
                string command = "SELECT AVG(TEMPURATURE) FROM WEATHER WHERE W_IDLOCATION = 1 AND (SELECT CONVERT(date, GETDATE())DATE_REGISTER) = @DATE";

                //SqlCommand
                SqlCommand cmm = new SqlCommand(command, cnn);

                //Opening connection
                cnn.Open();

                //Add the date to the query
                cmm.Parameters.AddWithValue("@DATE", today);

                //using the reader
                using (SqlDataReader reader = cmm.ExecuteReader())
                {
                    //While he reads
                    while (reader.Read())
                    {
                        //Add the avg to the temp
                        tempPorto = reader.GetDouble(0);
                    }
                }
            }

            //Returning temp
            return tempPorto;
        }

        [WebMethod]
        public double LisbonAvarageToday()
        {
            //Variables
            double tempLisbon = 0;
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=WeAreTooLate;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Seing date time today
            DateTime today = DateTime.Today.Date;

            //Sql connection using
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Command sql
                string command = "SELECT AVG(TEMPURATURE) FROM WEATHER WHERE W_IDLOCATION = 2 AND (SELECT CONVERT(date, GETDATE())DATE_REGISTER) = @DATE";
                
                //Creating a sql command
                SqlCommand cmm = new SqlCommand(command, cnn);

                //Opening connection
                cnn.Open();

                //Adding parameters
                cmm.Parameters.AddWithValue("@DATE", today);

                //Using sql data reader
                using (SqlDataReader reader = cmm.ExecuteReader())
                {
                    //While he haves data in
                    while (reader.Read())
                    {
                        //Adding the data to the variable
                        tempLisbon = reader.GetDouble(0);
                    }
                }
            }

            //Returning the value
            return tempLisbon;
        }

        [WebMethod]
        public double FaroAvarageToday()
        {
            //Variables
            double tempFaro = 0;
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=WeAreTooLate;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Seing date time today
            DateTime today = DateTime.Today.Date;

            //Using sql connection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //String to the sql command
                string command = "SELECT AVG(TEMPURATURE) FROM WEATHER WHERE W_IDLOCATION = 3 AND (SELECT CONVERT(date, GETDATE())DATE_REGISTER) = @DATE";

                //Sql command object
                SqlCommand cmm = new SqlCommand(command, cnn);

                //Openning connection
                cnn.Open();

                //Adding parameters
                cmm.Parameters.AddWithValue("@DATE", today);

                //Using sql data reader
                using (SqlDataReader reader = cmm.ExecuteReader())
                {
                    //While he have data in to read
                    while (reader.Read())
                    {
                        //temp from the sql
                        tempFaro = reader.GetDouble(0);
                    }
                }
            }

            //returing the value
            return tempFaro;
        }

        [WebMethod]
        public double AvarageWeek(DateTime firstDate, DateTime secoundDate, int idLocation)
        {
            //Variables
            double avarage = 0;
            string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=WeAreTooLate;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            //Using sql connection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //String to the sql command
                string command = "SELECT AVG(TEMPURATURE) FROM WEATHER WHERE W_IDLOCATION = @IDLOCATION AND DATE_REGISTER BETWEEN @DATEONE AND @DATESECOUND";

                //Sql command object
                SqlCommand cmm = new SqlCommand(command, cnn);

                //Openning connection
                cnn.Open();

                //Adding parameters
                cmm.Parameters.AddWithValue("@DATEONE", firstDate);
                cmm.Parameters.AddWithValue("@DATESECOUND", secoundDate);
                cmm.Parameters.AddWithValue("@IDLOCATION", idLocation);

                //Using sql data reader
                using (SqlDataReader reader = cmm.ExecuteReader())
                {
                    //While he have data in to read
                    while (reader.Read())
                    {
                        //Data from the web service to the variable
                        avarage = reader.GetDouble(0);
                    }
                }
            }

            //Returning value
            return avarage;
        }
    }
}
