﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Data.SqlClient;

namespace AppToSaveInTheDb
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creating a new object of my web service
            MyWebS.CallingWebSoapClient myWeb = new MyWebS.CallingWebSoapClient();

            //Variables
            int i = 0;

            //Variable to show the final hour and time that the program was working
            DateTime todayFinal = new DateTime();

            //Infinity cicle ti save in the db
            do
            {
                //Variable of hours now and the hours for the saving 
                int todayYear = DateTime.Now.Year;
                int todayMonth = DateTime.Now.Month;
                int todayDay = DateTime.Now.Day;
                int todayHour = DateTime.Now.Hour;
                int todayMin = DateTime.Now.Minute;
                int todaySec = DateTime.Now.Second;
                TimeSpan firstHour = new TimeSpan(00, 00, 00);
                TimeSpan secoundHour = new TimeSpan(08, 00, 00);
                TimeSpan tridHour = new TimeSpan(12, 00, 00);
                TimeSpan fourthHour = new TimeSpan(17, 00, 00);

                //Creating the new date time of today
                DateTime today = new DateTime(todayYear, todayMonth, todayDay, todayHour, todayMin, todaySec);

                //Displaying the hour that the program has execute
                Console.WriteLine(today);

                //Checking if it is time to save in the db
                if ((todayHour == 00 && todayMin == 00 && todaySec == 00) || (todayHour == 08 && todayMin == 00 && todaySec == 00) || (todayHour == 12 && todayMin == 00 && todaySec == 00) || (todayHour == 17 && todayMin == 00 && todaySec == 00)) //Test || (todayHour == 20 && todayMin == 56 && todaySec == 00))
                {
                    //connection string to the db 
                    string connectionString = "Data Source=oldbankcapela.database.windows.net;Initial Catalog=WeAreTooLate;User ID=capela_;Password=28021989Pi;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

                    //Variables to contain data from the web service
                    double portoTemp = myWeb.PortoNow();
                    double lisbonTemp = myWeb.LisbonNow();
                    double faroTemp = myWeb.FaroNow();

                    //Using sql connection
                    using (SqlConnection cnn = new SqlConnection(connectionString))
                    {
                        //Command to add to the location 1 
                        string commandPorto = "INSERT INTO WEATHER (ID, DATE_REGISTER, TEMPURATURE, W_IDLOCATION) VALUES ((SELECT MAX(ID) FROM WEATHER)+1, @DATE, @TEMP, 1)";

                        //Sql command object
                        SqlCommand cmmP = new SqlCommand(commandPorto, cnn);

                        //Opening connection
                        cnn.Open();

                        //Parameters 
                        cmmP.Parameters.AddWithValue("@DATE", today);
                        cmmP.Parameters.AddWithValue("@TEMP", portoTemp);

                        //Execute non query 
                        cmmP.ExecuteNonQuery();

                        //Closing connection
                        cnn.Close();

                        //Lisbon
                        string commandLisbon = "INSERT INTO WEATHER (ID, DATE_REGISTER, TEMPURATURE, W_IDLOCATION) VALUES ((SELECT MAX(ID) FROM WEATHER)+1, @DATE, @TEMP, 2)";

                        //Sql command object
                        SqlCommand cmmL = new SqlCommand(commandLisbon, cnn);
                        
                        //Opening connection
                        cnn.Open();

                        //Adding parameters
                        cmmL.Parameters.AddWithValue("@DATE", today);
                        cmmL.Parameters.AddWithValue("@TEMP", lisbonTemp);

                        //Execute command that is not a query
                        cmmL.ExecuteNonQuery();

                        //Clossing connection
                        cnn.Close();

                        //Faro
                        string commandFaro = "INSERT INTO WEATHER (ID, DATE_REGISTER, TEMPURATURE, W_IDLOCATION) VALUES ((SELECT MAX(ID) FROM WEATHER)+1, @DATE, @TEMP, 3)";

                        //Sql command object
                        SqlCommand cmmF = new SqlCommand(commandFaro, cnn);

                        //Connection open
                        cnn.Open();

                        //Adding parameters
                        cmmF.Parameters.AddWithValue("@DATE", today);
                        cmmF.Parameters.AddWithValue("@TEMP", faroTemp);

                        //Execute que command in this case not a query
                        cmmF.ExecuteNonQuery();

                        //Clossing connection
                        cnn.Close();
                    }
                }
                else
                {
                    i = 0;
                }

                //In case of the cicle clossing the user can have the last time he was working
                todayFinal = today;
                //Test Console.WriteLine(todayFinal.ToString());

            } while (i != 10000);

            //Display the final time of the aplication
            Console.WriteLine(todayFinal.ToString());
            Console.ReadLine();
        }
    }
}